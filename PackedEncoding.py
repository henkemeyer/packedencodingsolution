
def PackString(input_string):
    new_str = ""
    prev_char = ""
    num_instances = 1

    # First check for valid input
    if input_string == "":
        raise Exception("Must supply a non-empty string")

    # Iterate thru every letter
    for char in input_string:
        # If the letter is the same as the one prior, increment the count
        if char == prev_char:
            num_instances += 1
        else:  # Otherwise, add the count and the previous letter to the new, packed, string
            if prev_char != "":
                new_str += str(num_instances)
                new_str += prev_char
                num_instances = 1
        # Now store the current character as the previous character
        prev_char = char

    # Take care of the last letter
    new_str += str(num_instances)
    new_str += prev_char
    return new_str

if __name__ == "__main__":
    main()

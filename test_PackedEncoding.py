import unittest
from PackedEncoding import PackString

class myPackedEncodingTest(unittest.TestCase):

    def testUnpackableString(self):
        assert(PackString("ABCDEF") == "1A1B1C1D1E1F")

    def testNullString(self):
        with self.assertRaises(Exception): PackString("")

    def testString1(self):
        assert (PackString("AABBCCDEEFF") == "2A2B2C1D2E2F")

    def testMixedCaseString(self):
        assert (PackString("ABBCCcccccDEEFF") == "1A2B2C5c1D2E2F")

    def testVeryPackableString3(self):
        assert (PackString("ZZZZZZZZZZZZ") == "12Z")


    def testNumericString3(self):
        assert (PackString("111223455555") == "3122131455")


if __name__ == '__main__':
    unittest.main()